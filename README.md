# ypp-sc-tools - YARRG and other tools from Special Circumstances

The [YARRG website](https://yarrg.chiark.net/intro)
(and upload service)
source code is in the `yarrg/` subdirectory.

Please feel free to
[file tickets in gitlab](https://gitlab.com/yarrg/ypp-sc-tools/-/issues)
if things aren't working right.
