package provide yarrglib 0.1;

proc manyset {list args} {
    foreach val $list var $args {
        upvar 1 $var my
        set my $val
    }
}

proc httpclientsetup {program} {
    ::http::config -urlencoding utf-8
    set ua [::http::config -useragent]
    debug "USERAGENT OLD \"$ua\""
    set ua [exec ./database-info-fetch useragentstringmap $ua \
		    $program 2>@ stderr]
    ::http::config -useragent $ua
    debug "USERAGENT NEW \"$ua\""
}
